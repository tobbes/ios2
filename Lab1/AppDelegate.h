//
//  AppDelegate.h
//  Lab1
//
//  Created by Tobias Ednersson on 2015-01-21.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

