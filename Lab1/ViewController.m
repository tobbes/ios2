//
//  ViewController.m
//  Lab1
//
//  Created by Tobias Ednersson on 2015-01-21.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Write:(id)sender {
    if([self.view.backgroundColor isEqual:[UIColor redColor]]){
        self.view.backgroundColor = [UIColor blueColor];
    }

    else {
        self.view.backgroundColor = [UIColor redColor];
    }
    
}
@end
